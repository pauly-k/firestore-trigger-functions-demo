import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

admin.initializeApp();

export const todoCreateHandler = async (
  snapshot: FirebaseFirestore.DocumentSnapshot,
  context: functions.EventContext
) => {
  await snapshot.ref.update({
    id: snapshot.id,
    updated: Date.now(),
    change_count: 1
  });
  return setTodoTitles(snapshot.id);
};

export const todoUpdatedHandler = async (
  change: functions.Change<FirebaseFirestore.DocumentSnapshot>,
  context: functions.EventContext
) => {
  const previousValue = change.before.data() || {};
  const newValue = change.after.data() || {};

  if (todoUnchanged(previousValue, newValue)) {
    return null;
  }

  await change.after.ref.update({
    id: change.after.id,
    change_count: (previousValue.change_count || 1) + 1,
    updated: Date.now()
  });

  return setTodoTitles(change.after.id);
};

export const todoDeletedHandler = async (
  snapshot: FirebaseFirestore.DocumentSnapshot,
  context: functions.EventContext
) => {
  return setTodoTitles(snapshot.id, true);
};

function todoUnchanged(before: any, after: any) {
  return before.title === after.title && before.markdwon === after.markdwon;
}

async function setTodoTitles(todoId: string, remove: boolean = false) {
  const todoDoc = await admin
    .firestore()
    .doc(`todos/${todoId}`)
    .get();

  const todo = todoDoc.data() as any;
  const titleData = {
    id: todo.id,
    title: todo.title,
    updated: todo.updated
  };

  const titlesDoc = await getTodoTitlesDoc();
  const val = titlesDoc.data() as any;
  let titles = val.titles as any[];

  const index = titles.findIndex(x => x.id === todo.id);

  if (remove) {
    titles = [...titles.slice(0, index), ...titles.slice(index + 1)];
  } else {
    if (index < 0) {
      titles = [...titles, titleData];
    } else {
      titles = [
        ...titles.slice(0, index),
        titleData,
        ...titles.slice(index + 1)
      ];
    }
  }

  return await titlesDoc.ref.update({
    titles,
    updated: Date.now()
  });
}

async function getTodoTitlesDoc() {
  const titlesDoc = await admin
    .firestore()
    .doc("todo_titles/titles")
    .get();
  if (!titlesDoc.exists) {
    await titlesDoc.ref.set({ titles: [], updated: Date.now() });
  }
  return admin
    .firestore()
    .doc("todo_titles/titles")
    .get();
}
