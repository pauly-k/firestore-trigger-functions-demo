import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { todoCreateHandler, todoUpdatedHandler, todoDeletedHandler } from "./handlers/todo.handlers";

admin.initializeApp();

export const onTodoCreated = functions.firestore
  .document("todos/{todoId}")
  .onCreate(todoCreateHandler)

export const onTodoUpdated = functions.firestore
  .document("todos/{todoId}")
  .onUpdate(todoUpdatedHandler);

export const onTodoDeleted = functions.firestore
  .document("todos/{todoId}")
  .onDelete(todoDeletedHandler);

