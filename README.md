# Firestore triggers demo

* Triggers add `updated` and `id` fields to todos.
* Triggers create and update `todo_titles/titles` docuent

Prevent infinite recursive upates by checking if `todo` fields have cahnged and returnin if not

```javascript
if (todoUnchanged(previousValue, newValue)) {
  return null;
}
```
